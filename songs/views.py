from django.http import HttpResponse
from django.shortcuts import render_to_response
import jsonpickle
import json
#import redis
import oauth2client
from django.views.decorators.csrf import csrf_exempt
from oauth2client.anyjson import simplejson
from oauth2client.client import *
#import for user models
from accounts.models import UserProfile 
from  mongoengine.django.auth import User
from utils import createUser

#required for csrf and google
import json
import random
import string
##########################
# Create your views here.
KEY='songs'


def hello(request):
	print dir()
	name = Trie.time
	html="<html><body>Hi %s how the hell are you no. %s </html></body>", name
	return HttpResponse(html)

def song(request,song_id=1):
	##return HttpResponse("<html><body>...........</body></html>")

	return render_to_response('song.html',{'song':Song.objects.get(id=song_id)})


def search_request(request):
	try:
		search_text=request.GET['search_text']
		songs=Song.objects.filter(song_name__contains=search_text)
	except KeyError:
		movie_search=request.GET['movie_name']
		songs=Song.objects.filter(movie_name__contains=movie_search)
	##return render_to_response('ajax_search.html',{'songs':songs})
	songList=[]
	for song in songs:
		songObject={}
		songObject['name']=song.song_name
		songObject['link']=song.song_link
		songList.append(songObject)

	response=HttpResponse(jsonpickle.encode(songList,unpicklable=False));
	response["Access-Control-Allow-Origin"] = "*"
	response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
	response["Access-Control-Max-Age"] = "1000"
	response["Access-Control-Allow-Headers"] = "*"
	return response
	return HttpResponse('function_callback('+ jsonpickle.encode(songList,unpicklable=False)+')',content_type="application/json")


def complete(r, prefix, count):
    results = []
    rangelen = 50
    start = r.zrank(KEY, prefix)
    print start;
    if not start:
        return []
    while len(results) != count:
        range = r.zrange(KEY, start, start + rangelen - 1)
        start += rangelen
        if not range or len(range) == 0:
            break
        for entry in range:
            minlen = min((len(entry), len(prefix)))
            if entry[0:minlen] != prefix[0:minlen]:
                count = len(results)
                break
            if entry[-1] == '*' and len(results) != count:
                results.append(entry[0:-1])
    return results

def getresult(r,prefix):
	results=[]
	print prefix;
	if (len(prefix.split(" "))==1):
		print "calling complete"
		return complete(r,prefix,50);
	for key in prefix.split(" "):
		r.zadd("keys",0,key)				##Strict Redis is a bit weird

	r.zinterstore("results",prefix.split(' '));
	results = r.zrange("results",0,-1)
	return results

def auto_redis(request):
	search_request=request.GET['search_text']
	r = redis.StrictRedis(host='localhost', port=6379, db=0)
	songs = getresult(r,search_request);
	##songs=complete(r,search_request,100);
	songList=[]
	for song in songs:
			songObject={}
			songObject['name']=song
			songObject['link']=r.hget('songtolink',song)
			songList.append(songObject)

	response=HttpResponse(jsonpickle.encode(songList,unpicklable=False));
	response["Access-Control-Allow-Origin"] = "*"
	response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
	response["Access-Control-Max-tokenAge"] = "1000"
	response["Access-Control-Allow-Headers"] = "*"
	return response
	return HttpResponse('function_callback('+ jsonpickle.encode(songList,unpicklable=False)+')',content_type="application/json")


def csrf(request):
	state = ''.join(random.choice(string.ascii_uppercase + string.digits)
                  for x in xrange(32))
  	request.session['csrf'] = state
  	response=HttpResponse(json.dumps("1x1image"),200);
  	response.set_cookie("csrftoken",state);
  	response["Access-Control-Allow-Origin"] = "http://localhost"
	response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
	response["Access-Control-Max-Age"] = "1000"
	response["Access-Control-Allow-Headers"] = "*"
	response["Access-Control-Allow-Credentials"]="true"
	return response;

@csrf_exempt
def sendtogoogle(request):
		  print request.method
		  print request.session;
		  ##this can check for csrf token
		  print "COOKIES in session"+request.session['csrf'];
		  print "COOKIES in request"+request.COOKIES["csrftoken"]
		  print "inside sendtogoogle";
		  #print request;
		  #gplus_id = request.args.get('gplus_id')
		  code = request.POST['code'];
		  print "code"+code;
		  session=request.session;
		  CLIENT_ID="181509372760.apps.googleusercontent.com";
		  try:
		    # Upgrade the authorization code into a credentials object
		    print os.getcwd();
		    clientfile=os.path.join(os.getcwd(),"songs/client_secrets.json");
		    oauth_flow = flow_from_clientsecrets(clientfile, scope='')
		    # print str(oauth_flow.redirect_uri);
		    oauth_flow.redirect_uri = 'postmessage'
		    credentials = oauth_flow.step2_exchange(code)
		  except FlowExchangeError:
		    response = make_response(
		        json.dumps('Failed to upgrade the authorization code.'), 401)
		    response.headers['Content-Type'] = 'application/json'
		    response["Access-Control-Allow-Origin"] = "http://localhost"
		    response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
		    response["Access-Control-Max-Age"] = "1000"
		    response["Access-Control-Allow-Headers"] = "*"

		    return response

		  # Check that the access token is valid.
		  
		  access_token = credentials.access_token
		  print "access_token ",access_token;
		  url = ('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s'
		         % access_token)
		  h = httplib2.Http()
		  result = json.loads(h.request(url, 'GET')[1])

		  url2='https://www.googleapis.com/plus/v1/people/'+result['user_id']+"?access_token="+access_token
		  print result;
		  people=json.loads(h.request(url2,'GET')[1])
		  print people;
		  displayname=people['displayName'];
		  imageurl=people['image']['url'];
		  
		  session['gplus_id']=result['user_id'];
		  user=UserProfile.objects.filter(gplus_id__contains=result['user_id']);
		  print type(user);
		  print len(user);

		  if (len(user) != 0) and (user is not None):
		  	visitor=user[0];
		  	print "*"*60
		  	print "you have visited kiwi snow "+ str(visitor.count) + " times";
		  	print "*"*60
		  	visitor.count=visitor.count+1;
		  	visitor.save();

		  else:
		  	user_id=createUser(displayname);
		  	visitor=UserProfile(gplus_id=result['user_id'],user_id=user_id,count=0);
			visitor.save();

		  if visitor.displayname is not None:
		  	visitor.displayname=displayname;
		  	visitor.imageurl=people['image']['url'];
		  	visitor.save();
		  
		  # If there was an error in the access token info, abort.
		  if result.get('error') is not None:
		    response = make_response(json.dumps(result.get('error')), 500)
		    response.headers['Content-Type'] = 'application/json'
		    return response
		  # Verify that the access token is used for the intended user.
		  # if result['user_id'] != gplus_id:
		  #   response = make_response(
		  #       json.dumps("Token's user ID doesn't match given user ID."), 401)
		  #   response.headers['Content-Type'] = 'application/json'
		  #   return response
		  # # Verify that the access token is valid for this app.
		  if result['issued_to'] != CLIENT_ID:
		    response = make_response(
		        json.dumps("Token's client ID does not match app's."), 401)
		    response.headers['Content-Type'] = 'application/json'
		    return response
		  stored_credentials = request.session.get('credentials')
		  print "stored_credentials",stored_credentials;
		  # stored_gplus_id = session.get('gplus_id')
		  print displayname,imageurl
		  if stored_credentials is not None:
		    response = HttpResponse(people,content_type="application/json")
		    # response.headers['Content-Type'] = 'application/json'
		    response["Access-Control-Allow-Origin"] = "http://localhost"
		    response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
		    response["Access-Control-Max-Age"] = "1000"
		    response["Access-Control-Allow-Headers"] = "*"
		    response["Access-Control-Allow-Credentials"]="true"
		    print "current user is already connected";
		    return response
		  # Store the access token in the session for later use.
		  request.session['credentials'] = credentials
		  # session['gplus_id'] = gplus_id
		  print request.session['credentials'];
		  
		  response = HttpResponse(json.dumps('Successfully connected user.',{'displayname':displayname,
		  	'imageurl':imageurl}));
		  response["Access-Control-Allow-Origin"] = "http://localhost"
		  response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
		  response["Access-Control-Max-Age"] = "1000"
		  response["Access-Control-Allow-Headers"] = "*"
		  response["Access-Control-Allow-Credentials"]="true"
		    
		  return response;
