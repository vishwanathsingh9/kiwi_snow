from django.db import models
from mongoengine import *
from upload.models import SongInfo
from accounts.meta import *


# Create your models here.
class AlbumMeta(EmbeddedDocument):
	album_id=IntField();
	likes=IntField();
	playedCount=IntField();


class Tags(EmbeddedDocument):
	tagname=StringField();
	tagused=IntField();


class Comment(EmbeddedDocument):
    text=StringField(max_length=256);
    commenter=EmbeddedDocumentField(UserMetaInformation);



class Album(Document):
    cover=URLField();
    album_name = StringField(max_length=256)
    description = StringField(max_length=256)
    userinfo=EmbeddedDocumentField(UserMetaInformation)
    songs=ListField(EmbeddedDocumentField(SongInfo))
    taglist=ListField(EmbeddedDocumentField(Tags))
    albumMetaInfo=EmbeddedDocumentField(AlbumMeta)
    commentbox=ListField(EmbeddedDocumentField(Comment))
    meta = {"db_alias": "test"}


