import json
import requests
import jsonpickle
from models import  Album
from django.template.loader import get_template
from django.template import Context
from django.http import Http404, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from upload.util import sendCORSresponse

# will remove following method as template generation for album detail page will reside in kiwi_ice
def albumPage(request, albumId):
    try:
        albumId = int(albumId)
    except ValueError:
        raise Http404()
    # fetch album Details
    response = requests.get("http://127.0.0.1:8000/album/albumDetail/"+str(albumId)) 
    albumSongs = json.loads(response.text)
    firstSong = albumSongs[0]
    albumTitle = firstSong['album_name']
    template=get_template('albumPage.html')
    
    html=template.render(Context({'albumSongs':albumSongs, 'albumTitle':albumTitle, 'albumId':albumId}))
    return HttpResponse(html)

def album_title(request, album_id):
    album = Album.objects.filter(album_id=album_id)
    response_data={}
    if album.exists():
        album = album[0]; 
        response_data['albumTitle'] = album.album_name
        
    response = HttpResponse(jsonpickle.encode(response_data,unpicklable=False), mimetype='application/json')
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "*"
    return  response;

def album_detail(request, album_id):
    print "am here";
    album = Album.objects.filter(album_id=album_id)
    albumDetail=[]
    for song in album:
        songObject={}
        songObject['song_id']=song.id
        songObject['song_name']=song.song_name
        songObject['song_link']=song.song_link
        songObject['album_name']=song.album_name
        albumDetail.append(songObject)
    
    response=HttpResponse(jsonpickle.encode(albumDetail,unpicklable=False), mimetype='application/json')
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "*"
    return  response;

@csrf_exempt
def album_upload(request):
    if request.method == 'POST':
        print "final albumUpload";
        print request.POST['albumTitle'];
        print request.POST['descryption'];
        #get server locations of the file uploaded by the user in this album
        for uploadedSongs in request.session['uploadedList']:
            print uploadedSongs.docfile;
        return sendCORSresponse(None);

def stream(request,songname):
    print songname;
    song=Album.objects(album_name="naaam")[0]
    filedata=song.songs[0].musicfile.read()
    response=HttpResponse(filedata,mimetype='audio/mpeg ');
    return sendCORSresponse(response);
    