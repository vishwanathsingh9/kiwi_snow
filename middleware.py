""" use this class for handling input and output requests CSRF tokens """
from django.http import  HttpResponseNotFound
from django.http import HttpResponse
import random
import string
"""standard function to get csrf token (can improve later )""" 
def get_csrftoken():
        csrf = ''.join(random.choice(string.ascii_uppercase + string.digits)
                  for x in xrange(32)) #this is fine for now
        return csrf 
         


class CSRF:
    #
    def process_request(self , request):
        if (request.method in ['POST','PUT','DELETE'] ):
            session = request.session
            if '_csrf' not in session :
                return HttpResponseNotFound('<h1>Page not found</h1>')
            else :
                if session['_csrf'] == request.META['HTTP_X_CSRF_TOKEN'] :
                    return None
                else :
                    return HttpResponseNotFound('<h1>Page not found</h1>')
        else :
            return None
    
    #leaving this method empty for future use
    #def process_response(self , request , response ) :


       # if request.method == 'GET':
        #    session = request.session
         #   if 'csrftoken' in session :
class CORS:
    def process_request(self , request):
        if request.method == 'OPTIONS':  #handlng the OPTIONS method
            response =  HttpResponse(200)
            response["Access-Control-Allow-Origin"] = "http://localhost"
            response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
            response["Access-Control-Max-Age"] = "1000"
            response["Access-Control-Allow-Headers"] = "Content-Type,X-CSRF-Token"
            response["Access-Control-Allow-Credentials"] = "true"
            return response
        else :
            return None
            

    def process_response(self , request , response):
        response["Access-Control-Allow-Origin"] = "http://localhost"
        response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
        response["Access-Control-Max-Age"] = "1000"
        response["Access-Control-Allow-Headers"] = "Content-Type,X-CSRF-Token"
        response["Access-Control-Allow-Credentials"]="true"
        return response


