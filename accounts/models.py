# Create your models here.
from album.models import AlbumMeta
from mongoengine import *
from  mongoengine.django.auth import User
from meta import *

class UserProfile(Document):
    user = ReferenceField(User);
    metaInfo=EmbeddedDocumentField(UserMetaInformation);
    gplus_id=StringField();
    fb_id=StringField();
    count=IntField(default=0);
    username=StringField(max_length=50);
    gender=StringField(max_length=6);
    followers=ListField(EmbeddedDocumentField(UserMetaInformation));
    following=ListField(EmbeddedDocumentField(UserMetaInformation));
    collection=ListField(EmbeddedDocumentField(AlbumMeta));