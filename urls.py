from django.conf.urls import patterns, include, url
from songs.views import *


# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'kiwi_snow.views.home', name='home'),
    # url(r'^kiwi_snow/', include('kiwi_snow.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

     url(r'^hello/', hello),
     url(r'^get/(?P<song_id>\d+)/$', song),
     url(r'^search/$',search_request),
	 url(r'^upload/',include('upload.urls')),
    # url(r'^auto/$',auto_redis),
     url(r'^auth/', include('auth.urls')),
     url(r'^plus',sendtogoogle),
     url(r'^csrf',csrf),
     url(r'^album/', include('album.urls')),
)
