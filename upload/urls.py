# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, url
import views

urlpatterns = patterns('',
    url(r'^up/$',views.up),
    url(r'^thanks/$',views.thanks),
    url(r'^save/$',views.saveUpload),
)
