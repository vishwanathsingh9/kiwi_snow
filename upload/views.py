# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse
import jsonpickle
from django.views.decorators.csrf import csrf_exempt
from models import SongInfo
from forms import AlbumForm
from album.models import *
from upload.util import *;
import subprocess,os
import json
import pdb
from accounts.models import UserProfile

@csrf_exempt
def up(request):
    # Handle file upload


    if request.method == 'POST':
        print "am doing post here";
        newdoc = SongInfo();
        newdoc.name=request.FILES['files[]'].name;
        newdoc.musicfile.new_file(filename=newdoc.name);#http://stackoverflow.com/questions/8516550/querying-mongodb-gridfs
        newdoc.musicfile.write(request.FILES['files[]'])
        newdoc.musicfile.close();
        # songfile=newdoc.save();
        saveUploadSession(request,newdoc);
        result=json.JSONEncoder().encode({'files':[{"name":newdoc.name,"size":newdoc.musicfile.length}]});
        response=HttpResponse(result,content_type="application/json");
        return sendCORSresponse(response);

   ##if its not a post request
    request.session['uploadedList']=[]
    request.session.modified = True;
    response=HttpResponse(200);
    return sendCORSresponse(response);


@csrf_exempt
def saveUpload(request):
	uploadedFileList=request.session['uploadedList'];
	import pdb;pdb.set_trace();
	userinfo=UserProfile.objects(fb_id=str(request.session['FB_id']))[0];
	tags=[];
	tags.append(Tags(tagname="tag1",tagused=0));
	commentbox=[];
	commentbox.append(Comment(text="Hi am awesome",commenter=userinfo.metaInfo));
	albumMetaInfo=AlbumMeta(album_id=405,likes=1000,playedCount=109);
	newAlbum=Album(userinfo=userinfo.metaInfo,album_name="naaam",description="descrioption",
		taglist=tags,albumMetaInfo=albumMetaInfo,commentbox=commentbox);

	for file in uploadedFileList:
		newAlbum.songs.append(file);

	newAlbum.save();
	return sendCORSresponse(None);


def thanks(request):
	name=request.GET['file'];
	absolutepath=os.path.join(r"E:\kiwi\kiwi_frost\common\media\mysongs",name);
	print absolutepath;
	args=["madplay","-T",absolutepath,"2>&1"];
	p = subprocess.Popen(args, stdout=subprocess.PIPE,universal_newlines=True)
	##result = p.communicate()[0].decode('ascii');
	##print result
	song="";
	movie="";
	actor="";
	singer="";
	genre="";

	for line in p.stdout:
		line=line.decode('ascii')
		print "**************"
		print line
		if len(line.decode('ascii').strip().split(":"))==1:
				continue
		else:
			field,value=line.strip().split(":")
			print field,value
			if(field=="Title"):
				song=value;
			elif(field=="Artist"):
				singer=value
			elif(field=="Year"):
				year=value
			elif(field=="Album"):
				year=value
			elif(field=="Genre"):
				genre=value


	return render_to_response('thanks.html',
		{'song':song,
		'movie':movie,
		'actor':actor,
		'singer':singer,
		'genre':genre
		},
	context_instance=RequestContext(request))
