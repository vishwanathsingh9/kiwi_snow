from mongoengine import *
from accounts.meta import UserMetaInformation

class SongMetaInfo(EmbeddedDocument):
	name=StringField();
	likes=IntField();
	played=IntField();
	meta = {"db_alias": "test"};


class SongInfo(EmbeddedDocument):
	title=StringField();
	year=StringField();
	musicfile = FileField();
	timesPlayed=IntField();
	mp3=URLField();
	imageurl=URLField();
	ratings=IntField();
	duration=FloatField();
	songmeta=EmbeddedDocumentField(SongMetaInfo);
	artists=ListField(EmbeddedDocumentField(UserMetaInformation));## assuming artists to have metainfo
	meta = {"db_alias": "test"};
