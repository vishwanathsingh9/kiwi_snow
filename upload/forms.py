# -*- coding: utf-8 -*-
from django import forms

class AlbumForm(forms.Form):
	album_id = forms.IntegerField()
	album_name = forms.CharField(max_length=256)
	song_name = forms.CharField(max_length=256)
	docfile = forms.FileField(
		label='Select a file',
		help_text='max. 42 megabytes'
	)	