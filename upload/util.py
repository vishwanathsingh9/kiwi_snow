from django.http import HttpResponse


def sendCORSresponse(response):
	if(response==None):
		response=HttpResponse(200);

	response["Access-Control-Allow-Origin"] = "http://localhost"
	response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
	response["Access-Control-Max-Age"] = "1000"
	response["Access-Control-Allow-Headers"] = "*"
	response["Access-Control-Allow-Credentials"]="true"
	return response;


def saveUploadSession(request,file):
	print "*"*60;
	print request.session;
	import pdb;pdb.set_trace();
	print "*"*60;
	try:
		uploadedList=request.session['uploadedList'];


	except KeyError:
		print "error has occured";
		uploadedList=[];
		
	# import pdb;pdb.set_trace();
	uploadedList.append(file);
	print uploadedList;
	request.session['uploadedList']=uploadedList;
	request.session.modified = True


def handle_uploaded_file(f):
    destination = open(f.name, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()

